#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>


#include "user.h"
#include "list.h"
#include "udpComm.h"

int handleUDP(int *udpSocket){

	int nread;
	char buffer[128];
	char command[128];

	struct sockaddr_in addr;
	socklen_t addrlen;

	memset((void*)&addr, (int) '\0', sizeof(addr));
	memset(buffer, '\0', 128);

	addrlen=sizeof(addr);

	/*recive from udp socket the join message*/
	nread=recvfrom(*udpSocket, buffer, 128, 0, (struct sockaddr*)&addr, &addrlen);
	if(nread==-1)
		exit(EXIT_FAILURE);

	sscanf(buffer, "%[^ ]", command);

	if(strcmp(command, "REG")==0){
		commREG(buffer, udpSocket, addr);


		/*exit(EXIT_SUCCESS); SO THAT MARIA CAN LEARN.
		NO EXIT IN MIDDLE OF CODE (THAT ARE NOT WARENTED FOR)*/
	}
    return 0;
}

int commREG(char* buffer, int* udpSocket, struct sockaddr_in addr){


    socklen_t addrlen;

	int ret;
	char output[1024];
	char aux[256];
    char parseUser[128];
    char contactPlaceHolder[50];

	user* udpUser;

    memset(parseUser, '\0', sizeof(parseUser));
    memset(contactPlaceHolder,'\0', 50);

    addrlen=sizeof(addr);

    sscanf(buffer, "REG %s", parseUser);

	/* this function creates a new user and inserts it in the family list */
	insertInList(parseUser);

    udpUser = lastUser;

	/* snp variable to determine whether we are the authorized snp or not*/
	if(snp==1){

        memset(aux, '\0', sizeof(aux));
        memset(output, '\0', sizeof(output));

        strcpy(output, "LST\n");

		while(udpUser != NULL){

            strcpy(contactPlaceHolder,udpUser->contact);
            strcpy(aux, strcat(contactPlaceHolder, "\n"));
            memset(contactPlaceHolder, '\0', 50);

            if (strlen(output)+strlen(aux) < 1024){
                strcat(output, aux);
            }
            else{
                ret=sendto(*udpSocket, output, strlen(output), 0, (struct sockaddr*)&addr, addrlen);
                if(ret==-1)exit(EXIT_FAILURE);
                memset((void*)&output, (int) '\0', sizeof(output));
            }

			udpUser = udpUser->nextUser;
		}

        strcat(output, "\n");
        ret=sendto(*udpSocket, output, strlen(output), 0, (struct sockaddr*)&addr, addrlen);
        if(ret == -1){
            printf("SHITSTORM!!!!\n");
            exit(0);
        }

	}else{
        acceptNew(buffer,addr);
	}

    return 0;

}

int BcastMyself(int joinSocket, struct  sockaddr_in addr){

    struct in_addr a;
    user * userInList;

    char buffer[256];
    char answer[3];

    int retVal;
    socklen_t addrlen;

    /*addr = (sockaddr_in) inAddr;*/

    userInList = lastUser;

    while(userInList != NULL){
        if((strcmp(userInList->name, globalMyUser.name) != 0) && (strcmp(userInList->name, myDns->name) != 0)){
            inet_pton(AF_INET, userInList->IP, (void*) &a);
            /*casts first usable name into in_addr struct form*/

            memset((void*)&addr, (int)'\0', sizeof(addr));
            addr.sin_family=AF_INET;
            addr.sin_port=htons(userInList->udpPort);
            addr.sin_addr = a;


            memset(buffer, (int) '\0', 256);
            snprintf(buffer, 256, "REG %s", globalMyUser.contact);


            retVal = sendto(joinSocket, buffer, 256, 0, (struct sockaddr*)&addr, sizeof(addr));

            if(retVal < 0){
                printf("Error in retVal Bcast");
                return -1;
            }


            inet_pton(AF_INET, globalMyUser.IP, (void*) &a);

            memset((void*)&addr, (int)'\0', sizeof(addr));
            addr.sin_family=AF_INET;
            addr.sin_port=htons(globalMyUser.udpPort);
            addr.sin_addr = a;

            addrlen = sizeof(addr);
            retVal = recvfrom(joinSocket, buffer, 256, 0, (struct sockaddr*)&addr, &addrlen);

            if(retVal < 0){
                printf("Error in RecvFrom Bcast");
                return -1;
            }

            retVal = sscanf(buffer, "%[^\n]", answer);
            answer[2] = '\0';

            if((retVal < 0) || (strcmp(answer, "OK") != 0)){
                printf("Error in brodcasting myself. WELP\n");
                return (-1);
            }
            printf("OK from %s.%s\n", userInList->name, userInList->surname);
        }
        userInList = userInList->nextUser;
    }

    return 0;
}


int acceptNew(char* buffer, struct sockaddr_in addr){

    struct in_addr a;

    char parseUser[50];
    char IPaddr[50];
    char ok[3];
    char nameThrowAway[50];
    int retval, tcpPortThrowAway, udpPort, sock;

    memset(IPaddr, '\0', 50);
    memset(nameThrowAway, '\0', 50);
    memset(parseUser, '\0', 50);

    retval = sscanf(buffer, "REG %s", parseUser);

    if(retval < 0){
        printf("Error accepting new user\n");
        exit(0);
    }

    //insertInList(parseUser);


    retval = sscanf(parseUser, "%[^;];%[^;];%d;%d", nameThrowAway, IPaddr, &tcpPortThrowAway, &udpPort);

    inet_pton(AF_INET, IPaddr, (void*) &a);
            /*casts first usable name into in_addr struct form*/


    memset((void*)&addr, (int)'\0', sizeof(addr));
    addr.sin_family=AF_INET;
    addr.sin_port=htons(udpPort);
    addr.sin_addr = a;

    strcpy(ok, "OK");

    sock = socket(AF_INET, SOCK_DGRAM, 0);

    retval = sendto(sock, ok, sizeof(ok), 0, (struct sockaddr*)&addr, sizeof(addr));

    if(retval < 0){
        printf("Error accepting new user\n");
        exit(0);
    }

    printf("Accepter user!\n");

    close(sock);
    return 0;
}



int unrFamily(){

//    user* userInList;
//
//    userInList = lastUser;
//
//    while(userInList != NULL){
//
//        if(userInList.name == globalMyUser.name){
//            userInList = userInList->nextUser;
//        }
//
//
//
//    }



}
