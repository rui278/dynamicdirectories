#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>


#include "user.h"
#include "list.h"
#include "commands.h"
#include "udpComm.h"

int saSocket;

int handleComands(user myUser, char * saAddr, char * saPortChar){

    char command[256];
    char *parseCommand;
    int retval, saPort, i=0;

    saPort = atoi(saPortChar);

    /*retrieves comand and stores to string command*/
    fgets(command, 256, stdin);

    for(i = 0; i < 256; i++){
        if(command[i] == '\n'){
            parseCommand = (char *) malloc((i+1)*sizeof(char));
            break;
        }
        if(command[i] == ' '){
            parseCommand = (char *) malloc((i+1)*sizeof(char));
            break;
        }
    }


    if(parseCommand == NULL){
        printf("Malloc on ParseCommand not succecefull");
        fflush(stdout);
        exit(EXIT_FAILURE);
    }

    retval = sscanf(command, "%s", parseCommand);

    if(retval != 1){
        printf("Error parsing command");
        exit(EXIT_FAILURE);
    }

    /*Creates udp Socket to communicate with SA*/
    saSocket = socket(AF_INET, SOCK_DGRAM,0);


    /*Switch Commands*/


    /*Join*/
    if(strcmp("join", parseCommand) == 0){
        if(joinCommand(myUser,saAddr, saPort) < 0){
            printf("Join Error");
            exit(EXIT_FAILURE);
        }
    }

    /*Leave*/
    else if(strcmp(parseCommand, "leave") == 0){
        if(leaveCommand(myUser, saAddr, saPort) < 0){
            printf("Leave Error");
            exit(EXIT_FAILURE);
        }
    }

    else if(strcmp(parseCommand, "find") == 0){
        if(findCommand(command, saAddr, saPort) < 0){
            printf("Leave Error");
            exit(EXIT_FAILURE);
        }
    }

    /*Exit*/
    else if(strcmp(parseCommand, "lst") == 0){
        if(exitCommand() < 0){
            printf("Exit Error");
            exit(EXIT_FAILURE);
        }

    }



    return 1;
}



int joinCommand(user myUser, char * saAddr, int saPort){

    struct hostent *h;
    struct in_addr *a;
    struct sockaddr_in addr;

    user * dnsUser;

    int joinSocket;
    int retval, outSize;

    int bcast = 1, i,n;

    socklen_t addrlen;

    char * output;
    char outputLarge[1024];
    char toParse[1024];
    char inbuffer[256];
    char userBuff[50];
    char answer[4];

    dnsUser = (user*) malloc(sizeof(user));
    if(dnsUser == NULL){
        printf("Malloc Error on dnsUser");
        exit(EXIT_FAILURE);
    }

    memset((void*)inbuffer, 0, 256);
    memset((void*)outputLarge, '\0', 1024);
    printf("In Join\n");


    /*gets host's ip adress*/
    if((h=gethostbyname(saAddr)) == NULL){
        printf("Gethostbyname Error");
        exit(EXIT_FAILURE);
    }

    /*casts first usable name into in_addr struct form*/
    a = (struct in_addr*)h -> h_addr_list[0];


    memset((void*)&addr, (int)'\0', sizeof(addr));
    addr.sin_family=AF_INET;
    addr.sin_port=htons(saPort);
    addr.sin_addr = *a;

    outSize = strlen(myUser.contact)*sizeof(char) + sizeof("REG ") -1 + 2; /* -1 porque  ele conta com o \0 no size do REG*/

    output = (char*) malloc(outSize);

    /* Create output string */
    snprintf(output, outSize, "%s %s", "REG", myUser.contact);

    printf("%s\n", output);


    /*Sends Register message to SA server*/
    retval = sendto(saSocket, output, strlen(output)*sizeof(char), 0, (struct sockaddr*)&addr, sizeof(addr));

    if(retval == -1){
        printf("Join Error\n");
        exit(EXIT_FAILURE);
    }

    printf("sent the user\n");

    addrlen = sizeof(addr);

    /*Retrieves response message from SA server*/

    retval = recvfrom(saSocket, inbuffer, 256, 0, (struct sockaddr*)&addr, &addrlen);

    if(retval == -1){
        printf("Join recv Error\n");
        exit(EXIT_FAILURE);
    }

    printf("%s\n", inbuffer);


    retval = sscanf(inbuffer, "%s %[^.].%[^;];%[^;];%d", answer, dnsUser->name, dnsUser->surname, dnsUser->IP, &dnsUser->udpPort);
    if ((retval != 5) || (strcmp(answer, "DNS") != 0)){
        printf("Error when matching SA's response to register.\n");
        exit(EXIT_FAILURE);
    }

    myDns = dnsUser;

    /*If user is authorised dns for username, than completes the user
    structure with details and then inserts it into surname users list.*/
    if (strcmp(dnsUser->name, myUser.name) == 0){

        dnsUser->tcpPort = myUser.tcpPort;
        strcpy(dnsUser->contact, myUser.contact);
        initListFromUser(dnsUser);
        snp = 1;
        return 1;
    }


    close(saSocket);
    /*If user is not authorized dns then will register with authorized
     dns and retrive surname users list*/

    /*create socket to talk to DNS*/

    joinSocket = socket(AF_INET, SOCK_DGRAM, 0);

    /*Create Addr struct for DNS*/
    inet_pton(AF_INET, dnsUser->IP, (void*) a);
    /*casts first usable name into in_addr struct form*/


    memset((void*)&addr, (int)'\0', sizeof(addr));
    addr.sin_family=AF_INET;
    addr.sin_port=htons(dnsUser->udpPort);
    addr.sin_addr = *a;
    addrlen = sizeof(addr);

    retval = sendto(joinSocket, output, strlen(output)*sizeof(char), 0, (struct sockaddr*)&addr, sizeof(addr));

    if(retval == -1){
        printf("Join recv Error\n");
        exit(EXIT_FAILURE);
    }


    retval = recvfrom(joinSocket, outputLarge, 1024, 0, (struct sockaddr*)&addr, &addrlen);


    if(retval == -1){
        printf("Join recv Error\n");
        exit(EXIT_FAILURE);
    }

    retval = sscanf(outputLarge, "%[^\n]\n%[^ ]", answer, toParse);


    if(retval != 2){
        printf("Could not Parse LST from SNP\n");
        exit(EXIT_FAILURE);
    }
    answer[3] = '\0';

    if(strcmp(answer, "LST") == 0){
        bcast = 1;

        while(bcast){
            n=-1;
            for(i = 0; i < 1023; i++){
                n++;
                if(toParse[i] == '\n'){

                    insertInList(userBuff);
                    memset(userBuff,'\0', 50);
                    n=-1;

                    if(toParse[i+1] == '\n'){
                        bcast = 0;
                        toParse[i+1] = '\0';
                        break;
                    }else{
                        continue;
                    }
                }

                userBuff[n%50] = toParse[i];
            }

            if(bcast){
                    retval = recvfrom(joinSocket, outputLarge, 1024, 0, (struct sockaddr*)&addr, &addrlen);

                    if(retval == -1){
                        printf("Join->LST recv Error\n");
                        exit(EXIT_FAILURE);
                    }

                    retval = sscanf(outputLarge, "%[^ ]\n%[^ ]", answer, toParse);
                    answer[3] = '\0';
                    if (strcmp(answer, "LST") != 0){
                        printf("Bad format in Join->LST. Expected LST\n[RemainderUsers], got \"%s\" instead\n", answer);
                    }
            }
        }

        retval = BcastMyself(joinSocket, addr);

        if(retval < 0){
            printf("Error uppon Join->BcastMyself");
            exit(0);
        }
    }

    printf("Done Bcasting\n");

    close(joinSocket);
    return 1;
}


int leaveCommand(user myUser, char * saAddr, int saPort){

    char sUNR[128];

    strcpy(sUNR, stringUNR(myUser));

    /*user that is not the SNP*/
    if(snp == 0){
        unrFamily((char*)sUNR);
    }

    else{
        unrSA(sUNR, saAddr, saPort);
    }

    return 1;
}


int exitCommand(){

    user * userIter;

    userIter = lastUser;

    while(userIter != NULL){
        printf("user: %s.%s\n", userIter->name, userIter->surname);
        userIter = userIter->nextUser;

    }


    return 1;
}

int findCommand(){


}
