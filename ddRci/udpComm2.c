#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>


#include "user.h"
#include "list.h"
#include "udpComm.h"

int handleUDP(int *udpSocket){

	int nread;
	char buffer[128];
	char command[128];

	struct sockaddr_in addr;
	socklen_t addrlen;

	memset((void*)&addr, (int) '\0', sizeof(addr));

	addrlen=sizeof(addr);

	/*recive from udp socket the join message*/
	nread=recvfrom(*udpSocket, buffer, 128, 0, (struct sockaddr*)&addr, &addrlen);
	if(nread==-1)
		exit(EXIT_FAILURE);

	sscanf(buffer, "%[^ ]", command);

	if(strcmp(command, "REG")==0){
		commREG(buffer, udpSocket, addr);


		exit(EXIT_SUCCESS);
	}
    return 0;
}

int commREG(char* buffer, int* udpSocket, struct sockaddr_in addr){


    socklen_t addrlen;

	int ret;
	char output[1024];
	char aux[256];

	user* udpUser;


	udpUser = lastUser;

    addrlen=sizeof(addr);

	/* this function creates a new user and inserts it in the family list */
	insertInList(buffer);

	/* snp variable to determine whether we are the authorized snp or not*/
	if(snp==1){

        strcpy(output, "LST\n");

		while(udpUser->nextUser != NULL){


            strcpy(aux, strcat(udpUser->contact, "\n"));

            if (sizeof(output)+sizeof(aux) < 1024){
                strcat(output, aux);
            }
            else{
                ret=sendto(*udpSocket, output, strlen(output), 0, (struct sockaddr*)&addr, addrlen);
                if(ret==-1)exit(EXIT_FAILURE);
                memset((void*)&output, (int) '\0', sizeof(output));
            }

			udpUser = udpUser->nextUser;


		}

	}

    return 0;

}

int BcastMyself(int joinSocket, struct  sockaddr_in inAddr){

    struct in_addr a;
    struct sockaddr_in addr;
    user * userInList;

    char buffer[256];
    char answer[3];

    int retVal;
    socklen_t addrlen;

    /*addr = (sockaddr_in) inAddr;*/

    userInList = myDns;

    while(userInList != NULL){
        if((strcmp(userInList->name, globalMyUser.name) != 0) && (strcmp(userInList->name, myDns->name) != 0)){
            inet_pton(AF_INET, userInList->IP, (void*) &a);
            /*casts first usable name into in_addr struct form*/

            memset((void*)&addr, (int)'\0', sizeof(addr));
            addr.sin_family=AF_INET;
            addr.sin_port=htons(userInList->udpPort);
            addr.sin_addr = a;


            memset(buffer, (int) '\0', 256);
            snprintf(buffer, 256, "REG %s", userInList->contact);


            retVal = sendto(joinSocket, buffer, 256, 0, (struct sockaddr*)&addr, sizeof(addr));

            if(retVal < 0){
                printf("Error in retVal Bcast");
                return -1;
            }

            addrlen = sizeof(addr);
            retVal = recvfrom(joinSocket, buffer, 256, 0, (struct sockaddr*)&addr, &addrlen);

            if(retVal < 0){
                printf("Error in RecvFrom Bcast");
                return -1;
            }

            retVal = sscanf(buffer, "%[^\n]", answer);
            answer[2] = '\0';

            if((retVal < 0) || (strcmp(answer, "OK") != 0)){
                printf("Error in brodcasting myself. WELP\n");
                return (-1);
            }

        }
        userInList = userInList->nextUser;
    }

    return 0;
}

